module.exports = ctx => {

  const plugins = [
    require("postcss-normalize")({ forceImport: true }),
    require("postcss-preset-env")({ stage: 0 }),
    require("postcss-flexbugs-fixes")()
  ];

  if(ctx.webpack.mode === "production") {
    plugins.push(require("cssnano")({preset: "default"}));
  }

  return {
    plugins
  };

};

