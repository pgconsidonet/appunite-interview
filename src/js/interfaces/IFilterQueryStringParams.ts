/**
 * newsapi.org limited criteria API params
 */

export default interface IFilterQueryStringParams {
  q: string;
  sortBy: string;
  from?: string;
  to?: string;
}
