/**
 * Raw newsapi.org Article data model
 */
export default interface IRawArticle {

  source: {
    id: string;
    name: string;
  };

  author: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content: string;

}
