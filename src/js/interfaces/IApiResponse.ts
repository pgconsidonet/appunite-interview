/**
 * Universal API response interface
 */
export default interface IApiResponse {
  status: "ok" | "error";
  code?: string;
  message?: string;
}
