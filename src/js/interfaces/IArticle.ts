/**
 * Main Article data model
 */

import {Dayjs} from "dayjs";

export default interface IArticle {
  author: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: Dayjs;
  content: string;
  sourceLabel: string;
}
