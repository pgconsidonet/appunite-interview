/**
 * App filters data model
 */

export default interface IFilters {
  topic: string;
  sortBy: string;
  dates?: string;
}
