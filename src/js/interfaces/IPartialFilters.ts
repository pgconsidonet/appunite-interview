/**
 * App partial filters fragment for filters update/substitution
 */

export default interface IPartialFilters {
  topic?: string;
  sortBy?: string;
  dates?: string;
}
