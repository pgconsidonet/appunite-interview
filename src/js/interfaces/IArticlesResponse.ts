/**
 * API response data model for newsapi.org Articles query
 */

import IApiResponse from "./IApiResponse";
import IRawArticle from "./IRawArticle";

export default interface IArticlesResponse extends IApiResponse {
  totalResults: number;
  articles: IRawArticle[];
}
