import * as Redux from "redux";
import IArticle from "../../interfaces/IArticle";

export interface ISetArticleAction extends Redux.Action<"SET_ARTICLE"> {
  article?: IArticle;
}

/**
 * Sets article that is going to be displayed within Preview component
 * @param article
 */
export const setArticle = (article?: IArticle): ISetArticleAction => {
  return {
    type: "SET_ARTICLE",
    article
  }
};
