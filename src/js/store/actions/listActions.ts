import axios from "axios";
import * as Redux from "redux";
import dayjs from "dayjs";

import config from "../../config.json";
import IArticlesResponse from "../../interfaces/IArticlesResponse";
import IArticle from "../../interfaces/IArticle";
import IRawArticle from "../../interfaces/IRawArticle";
import IMainState from "../interfaces/IMainState";
import FilterHelpers from "../../helpers/FilterHelpers";
import IPartialFilters from "../../interfaces/IPartialFilters";
import IFilters from "../../interfaces/IFilters";

export interface IUpdateListAction extends Redux.Action<"UPDATE_LIST"> {
  items: IArticle[];
  replace: boolean;
  totalItems: number;
}

export interface ISetLoadingAction extends Redux.Action<"SET_LOADING"> {
  isLoading: boolean;
}

export interface ISetPageAction extends Redux.Action<"SET_PAGE"> {
  page: number;
}

export interface IUpdateFiltersAction extends Redux.Action<"UPDATE_FILTERS"> {
  partialFilters: IPartialFilters;
}

export interface IOverrideFiltersAction extends Redux.Action<"OVERRIDE_FILTERS"> {
  filters: IFilters;
}

/**
 * Internal action creator. Updates filtering/sorting criteria with a IPartialFilters fragment model.
 * @param partialFilters - contains only criteria that are supposed to be changed
 */
const updateFilterData = (partialFilters: IPartialFilters): IUpdateFiltersAction => {
  return {
    type: "UPDATE_FILTERS",
    partialFilters
  }
};

/**
 * Internal action creator. Replaces filtering/sorting criteria with a IFilters model.
 * @param filters - complete filtering criteria set
 */
const overrideFilterData = (filters: IFilters): IOverrideFiltersAction => {
  return {
    type: "OVERRIDE_FILTERS",
    filters
  }
};

/**
 * Internal action creator. Changes the internal page number (used to detect whether to hide the 'load more' button)
 * @param page
 */
const setPage = (page: number): ISetPageAction => {
  return {
    type: "SET_PAGE",
    page
  }
};


/**
 * Sets whether app is in loading mode. This state should be reflected with proper UI feedback.
 * @param isLoading
 */
export const setLoading = (isLoading: boolean): ISetLoadingAction => {
  return {
    type: "SET_LOADING",
    isLoading
  }
};

/**
 * Low-level action creator. Updates or substitues the loaded list depending on params
 * @param articlesResponse - contains API response with article list
 * @param criteriaChanged - determines whether criteria have been changed. If true, the list gets substituted instead of extended
 */
export const updateList = (articlesResponse: IArticlesResponse, criteriaChanged: boolean): IUpdateListAction => {
  return {
    type: "UPDATE_LIST",
    items: articlesResponse.articles.map<IArticle>((item: IRawArticle): IArticle => ({
      author: item.author,
      sourceLabel: item.source.name,
      content: item.content,
      description: item.description,
      publishedAt: dayjs(item.publishedAt),
      title: item.title,
      url: item.url,
      urlToImage: item.urlToImage
    })),
    replace: criteriaChanged,
    totalItems: articlesResponse.totalResults
  };
};

/**
 * Loads (more) articles and extends or substitutes the loaded list depending on params
 * @param criteriaChanged - determines whether criteria have been changed. If true, the list gets substituted instead of extended
 */
export const loadData = (criteriaChanged: boolean = false): ((dispatch: Redux.Dispatch, getState: (() => IMainState)) => Promise<void>) => {
  return async (dispatch: Redux.Dispatch, getState: (() => IMainState)): Promise<void> => {

    // To provide the loading indicator
    dispatch(setLoading(true));

    // Getting current state (page number and chosen filters)
    const listState = getState().list;

    // If criteria changed - page number needs to be reset; otherwise we increment the page number
    const page = criteriaChanged ? 1 : listState.loadedPages + 1;
    dispatch(setPage(page));

    // Common params
    const apiParams = {
      apiKey: config.feed.apiKey,
      pageSize: config.ui.pageSize,
      language: config.feed.language,
      page
    };

    // Filter params based on selection in the UI
    const filterParams = FilterHelpers.getQueryStringParams(listState.filters);

    // HTTP request
    try {
      const response = await axios.request<IArticlesResponse>({
        url: config.feed.url,
        params: {...apiParams, ...filterParams}
      });
      dispatch(updateList(response.data, criteriaChanged));
    } catch (error) {
      throw (error);
    }

  }
};

/**
 * Updates filtering/sorting criteria with a IPartialFilters fragment model and triggers list reload
 * @param partialFilters - contains only criteria that are supposed to be changed
 */
export const updateFilters = (partialFilters: IPartialFilters): ((dispatch: Redux.Dispatch) => void) => {
  return (dispatch: Redux.Dispatch): void => {

    // Here we take the advantage of classic dispatches that are synchronous by design

    // Updating meta
    dispatch(updateFilterData(partialFilters));

    // Loading more
    dispatch<any>(loadData(true));

  };
};

/**
 * Resets all the criteria to the default setup and triggers list reload
 */
export const clearFilters = (): ((dispatch: Redux.Dispatch) => void) => {
  return (dispatch: Redux.Dispatch): void => {

    // Here we take the advantage of classic dispatches that are synchronous by design

    // Updating meta
    dispatch(overrideFilterData(config.ui.defaultFilters));

    // Loading more
    dispatch<any>(loadData(true));

  };
};
