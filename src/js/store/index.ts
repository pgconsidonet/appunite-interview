import * as Redux from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

import IMainState from "./interfaces/IMainState";
import * as MainReducers from "./reducers";

export default Redux.createStore(
  Redux.combineReducers<IMainState>({
    list: MainReducers.listReducer,
    preview: MainReducers.previewReducer
  }),
  composeWithDevTools(
    Redux.applyMiddleware(thunk),
  )
);
