import * as Redux from "redux";
import initState from "../initState";
import IPreviewState from "../interfaces/IPreviewState";
import {ISetArticleAction} from "../actions/previewActions";

export default (state: IPreviewState = initState.preview, action: Redux.AnyAction): IPreviewState => {

  switch (action.type) {

    case "SET_ARTICLE":
      return {
        ...state,
        article: (action as ISetArticleAction).article
      };

    default:
      return state;

  }

};
