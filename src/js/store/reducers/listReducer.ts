import * as Redux from "redux";
import config from "../../config.json";
import initState from "../initState";
import IListState from "../interfaces/IListState";
import {
  IOverrideFiltersAction,
  ISetLoadingAction,
  ISetPageAction,
  IUpdateFiltersAction,
  IUpdateListAction
} from "../actions/listActions";

export default (state: IListState = initState.list, action: Redux.AnyAction): IListState => {

  switch (action.type) {

    case "SET_LOADING":
      return {
        ...state,
        isLoading: (action as ISetLoadingAction).isLoading
      };

    case "SET_PAGE":
      return {
        ...state,
        loadedPages: (action as ISetPageAction).page
      };

    case "UPDATE_FILTERS":
      return {
        ...state,
        filters: {
          ...state.filters,
          ...(action as IUpdateFiltersAction).partialFilters
        }
      };

    case "OVERRIDE_FILTERS":
      return {
        ...state,
        filters: (action as IOverrideFiltersAction).filters
      };

    case "UPDATE_LIST":

      const updateListAction = action as IUpdateListAction;

      return {
        ...state,
        items: updateListAction.replace ? updateListAction.items : [...state.items, ...updateListAction.items],
        pageCount: Math.ceil(updateListAction.totalItems / config.ui.pageSize),
        isLoading: false
      };

    default:
      return state;

  }

};
