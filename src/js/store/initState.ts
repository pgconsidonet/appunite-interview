/**
 * Initial state
 */
import config from "../config.json";
import IMainState from "./interfaces/IMainState";

const initState: IMainState = {
  list: {
    items: [],
    loadedPages: 0,
    pageCount: 0,
    filters: config.ui.defaultFilters,
    isLoading: false
  },
  preview: {
    article: undefined
  }
};

export default initState;
