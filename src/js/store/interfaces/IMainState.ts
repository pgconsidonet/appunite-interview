/**
 * Main root state. Contains all sub-states.
 */
import IListState from "./IListState";
import IPreviewState from "./IPreviewState";

export default interface IMainState {
  list: IListState;
  preview: IPreviewState;
}
