/**
 * List state. Contains all the information regarding the article list browser.
 */
import IArticle from "../../interfaces/IArticle";
import IFilters from "../../interfaces/IFilters";

export default interface IListState {
  items: IArticle[];
  pageCount: number;
  loadedPages: number;
  isLoading: boolean;
  filters: IFilters;
}
