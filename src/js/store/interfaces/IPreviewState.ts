/**
 * Preview state. Contains all the information regarding the article preview.
 */
import IArticle from "../../interfaces/IArticle";

export default interface IPreviewState {
  article?: IArticle;
}
