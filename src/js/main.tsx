import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import mainStore from "./store";

import App from "./components/App";

document.addEventListener("DOMContentLoaded", (): void => {

  ReactDOM.render(
    <Provider store={mainStore}>
      <App />
    </Provider>,
    document.getElementById("app")
  );

});
