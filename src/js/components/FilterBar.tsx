import React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import {boundMethod} from "autobind-decorator";

import IMainState from "../store/interfaces/IMainState";
import IListState from "../store/interfaces/IListState";
import IPartialFilters from "../interfaces/IPartialFilters";

import {updateFilters, clearFilters} from "../store/actions/listActions";

import FilterPicker from "./partials/FilterPicker";

interface IFilterBarStateProps {
  state: IListState;
}

interface IFilterBarStateDispatch {
  dispatchSetFilter: (partialFilters: IPartialFilters) => void;
  dispatchClearFilters: () => void;
}

interface IFilterBarProps extends IFilterBarStateProps, IFilterBarStateDispatch {}

class FilterBar extends React.Component<IFilterBarProps> {

  @boundMethod
  public setDates(dates: string): void {
    this.props.dispatchSetFilter({ dates });
  }

  @boundMethod
  public setTopic(topic: string): void {
    this.props.dispatchSetFilter({ topic });
  }

  @boundMethod
  public setSortBy(sortBy: string): void {
    this.props.dispatchSetFilter({ sortBy });
  }

  public render(): React.ReactElement {
    return (<div className="filterbar">
      <input type="radio" name="filterpicker__state" className="filterbar__state" id="fp__none" />
      <div className="filterbar__filter">
        <FilterPicker
          optional={false}
          options={["tech", "travel", "politics", "sports"]}
          placeholder="Topic"
          value={this.props.state.filters.topic}
          onChange={this.setTopic}
        />
      </div>
      <div className="filterbar__filter">
        <FilterPicker
          optional={false}
          options={["popularity", "publishedAt"]}
          placeholder="Sort by"
          value={this.props.state.filters.sortBy}
          onChange={this.setSortBy}
        />
      </div>
      <div className="filterbar__filter">
        <FilterPicker
          optional={true}
          options={["this month", "this week"]}
          placeholder="Dates"
          value={this.props.state.filters.dates}
          onChange={this.setDates}
        />
      </div>
      <div className="filterbar__clear">
        <button onClick={this.props.dispatchClearFilters} className="button button--alt button--compact filterbar__button">Clear filters</button>
      </div>
    </div>);
  }

}

// It should be split into container and presentation component but with the size of this project I think it can be ignored
export default connect(
  (state: IMainState): IFilterBarStateProps => ({
    state: state.list
  }),
  (dispatch: Redux.Dispatch): IFilterBarStateDispatch => ({
    dispatchSetFilter: (partialFilters): void => dispatch<any>(updateFilters(partialFilters)),
    dispatchClearFilters: (): void => dispatch<any>(clearFilters())
  })
)(FilterBar);
