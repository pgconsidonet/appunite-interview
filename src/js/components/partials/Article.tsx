import React from "react";
import IArticle from "../../interfaces/IArticle";
import ArticleHeading from "./ArticleHeading";
import ArticleImage from "./ArticleImage";

interface IArticleProps {
  article: IArticle;
  onReadMore: (article: IArticle) => void;
}

const Article = (props: IArticleProps): React.ReactElement => (
  <article className="list__item article">
    <ArticleImage article={props.article}/>
    <ArticleHeading article={props.article}/>
    <h3 className="article__title">{props.article.title}</h3>
    <div className="article__description">{props.article.description}</div>
    <a onClick={(e): void => { e.preventDefault(); props.onReadMore(props.article); }} href="#" className="article__button button">Read More</a>
  </article>
);

export default Article;
