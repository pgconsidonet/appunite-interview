import React from "react";
import uniqid from "uniqid";
import classNames from "classnames";

interface IFilterPickerProps {
  options: string[];
  placeholder: string;
  value: string | undefined;
  onChange: ((value: any) => void);
  optional: boolean;
}

interface IFilterPickerState {
  id: string;
}

export default class FilterPicker extends React.Component<IFilterPickerProps, IFilterPickerState> {

  public readonly state = {
    id: uniqid("fp__")
  };

  public render(): React.ReactElement {
    return (<div className="filterpicker">

      <input type="radio" name="filterpicker__state" className="filterpicker__state" id={this.state.id} />

      <div className="filterpicker__label">
        <label htmlFor={this.state.id} className="filterpicker__opener">{typeof this.props.value !== "undefined" ? `${ this.props.placeholder}: ${this.props.value}` : this.props.placeholder}</label>
        <label htmlFor="fp__none" className="filterpicker__closer" />
      </div>

      <div className="filterpicker__options">
        {
          this.props.options.map((i): React.ReactElement => (
            <label
              htmlFor="fp__none"
              key={i}
              onClick={(): void => {

                if(this.props.optional && i === this.props.value) {
                  this.props.onChange(undefined);
                } else {
                  this.props.onChange(i);
                }

              }}
              className={classNames({
                "filterpicker__option": true,
                "filterpicker__option--is-selected": i === this.props.value
              })}
            >{i}</label>))
        }
      </div>

    </div>);
  }

}
