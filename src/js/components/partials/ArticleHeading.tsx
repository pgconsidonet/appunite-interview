import React from "react";
import IArticle from "../../interfaces/IArticle";

interface IArticleHeadingProps {
  article: IArticle;
}

const ArticleHeading = (props: IArticleHeadingProps): React.ReactElement => (
  <div className="article__heading">
    <div className="article__heading-item article__param">{props.article.publishedAt.format("MMM D, YYYY")}</div>
    <div className="article__heading-item article__param">{props.article.author}</div>
    <a href={props.article.url} rel="noopener noreferrer" target="_blank" className="article__heading-item article__param article__param--source">{props.article.sourceLabel}</a>
  </div>
);

export default ArticleHeading;
