import React from "react";
import IArticle from "../../interfaces/IArticle";

interface IArticleImageProps {
  article: IArticle;
}

const ArticleImage = (props: IArticleImageProps): React.ReactElement => (
  <div className="article__image" style={ props.article.urlToImage !== null ? {"backgroundImage": `url('${props.article.urlToImage}')`} : undefined} />
);

export default ArticleImage;
