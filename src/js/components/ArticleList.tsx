import React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";
import {boundMethod} from "autobind-decorator";
import classNames from "classnames";

import IMainState from "../store/interfaces/IMainState";
import IListState from "../store/interfaces/IListState";
import IPreviewState from "../store/interfaces/IPreviewState";
import IArticle from "../interfaces/IArticle";

import {loadData} from "../store/actions/listActions";
import Article from "./partials/Article";
import {ISetArticleAction, setArticle} from "../store/actions/previewActions";

interface IArticleListStateProps {
  listState: IListState;
  previewState: IPreviewState;
}

interface IArticleListStateDispatch {
  dispatchLoadData: () => void;
  dispatchOpenPreview: (article: IArticle) => void;
}

interface IArticleListProps extends IArticleListStateProps, IArticleListStateDispatch {}

class ArticleList extends React.Component<IArticleListProps> {

  public componentDidMount(): void {
    this.props.dispatchLoadData();
  }

  @boundMethod
  public doLoadMore(): void {
    this.props.dispatchLoadData();
  }

  public render(): React.ReactElement {

    return (<div className={classNames({
      "list": true,
      "list--is-loading": this.props.listState.isLoading
    })}>
      <div className="list__list">
        {
          this.props.listState.items.map((item, idx): React.ReactElement => <Article key={idx} article={item} onReadMore={this.props.dispatchOpenPreview} />)
        }
      </div>

      <div className="list__footer">
        {
          // Conditional 'load more' - only if there are pages to load
          (this.props.listState.loadedPages < this.props.listState.pageCount) ? <button className="button button--alt list__loadmore-button" onClick={this.doLoadMore}>Show More</button> : null
        }

      </div>

    </div>);
  }

}

// It should be split into container and presentation component but with the size of this project I think it can be ignored
export default connect(
  (state: IMainState): IArticleListStateProps => ({
    listState: state.list,
    previewState: state.preview
  }),
  (dispatch: Redux.Dispatch): IArticleListStateDispatch => ({
    dispatchLoadData: (): void => dispatch<any>(loadData()),
    dispatchOpenPreview: (article: IArticle): ISetArticleAction => dispatch(setArticle(article))
  })
)(ArticleList);
