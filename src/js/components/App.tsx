import React from "react";
import FilterBar from "./FIlterBar";
import ArticleList from "./ArticleList";
import Preview from "./Preview";

export default class App extends React.Component {

  public render(): React.ReactElement {
    return (
      <main className="main">
        <h1 className="main__h1">Articles</h1>
        <FilterBar />
        <ArticleList />
        <Preview />
      </main>
    )
  }

}
