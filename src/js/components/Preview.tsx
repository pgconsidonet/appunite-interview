import React from "react";
import * as Redux from "redux";
import { connect } from "react-redux";

import IMainState from "../store/interfaces/IMainState";
import IArticle from "../interfaces/IArticle";

import {setArticle} from "../store/actions/previewActions";
import config from "../config.json";

import ArticleImage from "./partials/ArticleImage";
import ArticleHeading from "./partials/ArticleHeading";

interface IPreviewStateProps {
  article?: IArticle;
}

interface IPreviewStateDispatch {
  dispatchClosePreview: () => void;
}

interface PreviewProps extends IPreviewStateProps, IPreviewStateDispatch {}

class Preview extends React.Component<PreviewProps> {

  public render(): React.ReactElement | null {
    return typeof this.props.article === "undefined" ? null : (
      <div className="main__popup">
        <div className="main__popup-inner">
          <article className="preview article">

            <h2 className="main__h2">Article</h2>
            <div className="preview__back">
              <a href="#" className="preview__back-link" onClick={(e): void => {
                this.props.dispatchClosePreview();
                e.preventDefault();
              }}>Return to article list</a>
            </div>

            <ArticleImage article={this.props.article}/>

            <div className="preview__contents">
              <ArticleHeading article={this.props.article}/>

              <h3 className="article__title">{this.props.article.title}</h3>
              <div className="article__description">{this.getTruncatedContents()}</div>

              <div className="preview__button-wrapper">
                <a href={this.props.article.url} rel="noopener noreferrer" target="_blank" className="button">Go To Source</a>
              </div>

            </div>

          </article>
        </div>
      </div>
    );
  }

  private getTruncatedContents(): string {

    if(typeof this.props.article !== "undefined") {

      const maxLen = config.ui.contentAbstractLength as number;
      const len = this.props.article.content.length;

      return len > maxLen ? `${this.props.article.content.substring(0, maxLen)}... [+${len - maxLen} chars]` : this.props.article.content;

    }

    return "";

  }

}

// It should be split into container and presentation component but with the size of this project I think it can be ignored
export default connect(
  (state: IMainState): IPreviewStateProps => ({
    article: state.preview.article
  }),
  (dispatch: Redux.Dispatch): IPreviewStateDispatch => ({
    dispatchClosePreview: (): void => dispatch<any>(setArticle(undefined)),
  })
)(Preview);
