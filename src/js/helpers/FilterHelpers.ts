import dayjs from "dayjs";
import IFilters from "../interfaces/IFilters";
import IFilterQueryStringParams from "../interfaces/IFilterQueryStringParams";

interface IDateQueryPartial {
  from?: string;
  to?: string;
}

export default class FilterHelpers {

  /**
   * Parses a string value (`this month` or `this week`) into IDateQueryPartial object representing a dayjs time span
   * @param dateQuery
   */
  private static parseDate(dateQuery: string): IDateQueryPartial {

    const now = dayjs();

    switch (dateQuery) {

      case "this month":

        return {
          from: now.startOf("month").toISOString(),
          to: now.toISOString()
        };

      case "this week":

        return {
          from: now.startOf("week").toISOString(),
          to: now.toISOString()
        };

      default:
        return {};

    }

  }

  /**
   * Returns newsapi.org-compliant API params based on IFilters object
   * @param filters
   */
  public static getQueryStringParams(filters: IFilters): IFilterQueryStringParams {

    return {
      sortBy: filters.sortBy,
      q: filters.topic,
      ...this.parseDate(typeof filters.dates !== "undefined" ? filters.dates : "")
    }

  }

}
