import React from 'react';
import {shallow} from 'enzyme';
import dayjs from "dayjs";

import Article from "../js/components/partials/Article";
import ArticleHeading from "../js/components/partials/ArticleHeading";

const TEST_MODEL = {
  title: "test1",
  urlToImage: "http://localhost/image.png",
  publishedAt: dayjs("2019-08-08T19:07:59.914Z"),
  url: "http://localhost/article",
  sourceLabel: "Test Source",
  description: "test description",
  content: "test content",
  author: "test author"
};

describe("Article", () => {

  it("should render", () => {

    const articleComponent = shallow(<Article article={TEST_MODEL} />);

    // General layout snapshot
    expect(articleComponent.getElements()).toMatchSnapshot("Article initial snapshot");

  });

});

describe("ArticleHeading", () => {

  test("date-interpretation", () => {

    const articleHeadingComponent = shallow(<ArticleHeading article={TEST_MODEL} />);

    // Date interpretation
    expect(articleHeadingComponent.find(".article__heading-item.article__param").at(0).text()).toBe("Aug 8, 2019");

  });

});
