import mainStore from "../js/store";
import {updateList} from "../js/store/actions/listActions";
import config from "../js/config";

const MOCKED_TOTAL_RESULTS = 50;
const MOCKED_ARTICLES = [
  {
    title: "test1",
    urlToImage: "http://localhost/image.png",
    publishedAt: "2019-08-08T19:07:59.914Z",
    url: "http://localhost/article",
    source: {
      id: "test_source",
      name: "Test Source"
    },
    description: "test description",
    content: "test content",
    author: "test author"
  },
  {
    title: "test2",
    urlToImage: "http://localhost/image.png",
    publishedAt: "2019-08-08T19:07:59.914Z",
    url: "http://localhost/article",
    source: {
      id: "test_source",
      name: "Test Source"
    },
    description: "test description",
    content: "test content",
    author: "test author"
  }
];

mainStore.dispatch(updateList({
  status: "ok",
  totalResults: MOCKED_TOTAL_RESULTS,
  articles: MOCKED_ARTICLES
}, false));

const listState = mainStore.getState().list;

// Checking if there are all articles
test("article-amount", () => {
  expect(listState.items.length).toBe(2);
});

// Checking if page count is calculated properly
test("page-count", () => {
  expect(listState.pageCount).toBe(Math.ceil(MOCKED_TOTAL_RESULTS / config.ui.pageSize));
});

// Checking data are mapped properly
test("data-mapping", () => {

  const article = listState.items[0];

  expect(article.title).toBe(MOCKED_ARTICLES[0].title);
  expect(article.urlToImage).toBe(MOCKED_ARTICLES[0].urlToImage);
  expect(article.publishedAt.toISOString()).toBe(MOCKED_ARTICLES[0].publishedAt);
  expect(article.url).toBe(MOCKED_ARTICLES[0].url);
  expect(article.sourceLabel).toBe(MOCKED_ARTICLES[0].source.name);
  expect(article.description).toBe(MOCKED_ARTICLES[0].description);
  expect(article.content).toBe(MOCKED_ARTICLES[0].content);
  expect(article.author).toBe(MOCKED_ARTICLES[0].author);

});
