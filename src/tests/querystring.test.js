import FilterHelpers from "../js/helpers/FilterHelpers";
import MockDate from "mockdate";

test("querystring-nodates", () => {

  const params = FilterHelpers.getQueryStringParams({
    sortBy: "testSortBy",
    topic: "testQuery"
  });

  expect(params.sortBy).toBe("testSortBy");
  expect(params.q).toBe("testQuery");
  expect(params.from).toBe(undefined);
  expect(params.to).toBe(undefined);

});

test("querystring-with-dates", () => {

  MockDate.set("2019-05-20", 0);

  const params = FilterHelpers.getQueryStringParams({
    sortBy: "anotherTestSortBy",
    topic: "anotherTestQuery",
    dates: "this month"
  });

  expect(params.sortBy).toBe("anotherTestSortBy");
  expect(params.q).toBe("anotherTestQuery");
  expect(params.from).toBe("2019-04-30T22:00:00.000Z"); // Important: dayjs seems to ignore the mocked timezone setting. Therefore this test will be successful only in GMT+1 DST (UTC+2). Sorry :D
  expect(params.to).toBe("2019-05-20T00:00:00.000Z");

});
