# appunite-interview

> Recruitment interview task for AppUnite

Author: Paweł Gabryelewicz

## General notes

- TypeScript extensively used to handle data models and state management (which makes things a bit too complex with that kind of project size). I hope you don't hate it :D
- There are no styled components, I am not ready to use them. I have used classic SCSS+BEM instead.
- Change against the brief: resetting filter is possible only for 'Dates' (on purpose). This is due to the construction of the news API (at least topic has to be selected and the sort order is selected implicitly anyway)
- `dayjs` used instead of `momentjs`. This makes the final bundle dramatically smaller.
- 'this week' starts on Sunday (according to `en` locale). This can be changed by importing and loading [proper locale](https://github.com/iamkun/dayjs/blob/dev/docs/en/I18n.md).

## Simplified things

- Filter keys are pure strings (both visual labels and internal values). There are no constants there. This is just to show the general approach.
- Redux action types are also pure strings, not exported constants.
- Article detail page is just a div, without proper routing, to save time.
- `react-redux`-connected components are single files, not divided into container and presentation components. That was also done to make things more compact for the exercise.
- In particular Preview component should be a functional component with a container on top. This should also evolve into proper routing. Now the business logic and visual presentation is merge together into one component to save time.
- 'Read more' passes back the IArticle object, since there isn't any unique ID in the article list and there is no need to download the contents (they are available already when displaying the list)
- No thorough cross-browser tests were done. Proven to work on Webkit- and Gecko-based browsers
- Construction of most of procedures and state actions is relying in constantly importing configuration JSON. This saves time during development but makes this piece of software hard to test. In production environment the architecture of functions, actions and internal API should be as independent as possible, relying only on their parameters. This is for sure an area to improve here.

## Toolkit

- `webpack` is used to create the bundle.
- Both TypeScript and React is transpiled using `babel` version 7 (with usage-based polyfilling and `core-js` 3). This setup seems to be much faster than Microsoft TypeScript compiler. But... see below.
- There is a type checking service integrated (both as a separate `npm` task and a `webpack` plugin) to provide instant feedback about typings. This doesn't affect Babel transpilation speed and is running in a completely different thread.
- SCSS is compiled using [`sass`](https://www.npmjs.com/package/sass) ([`dart-sass`](https://sass-lang.com/dart-sass))
- `postcss` is used to generate cross-browser CSS (autoprefixing and unsupported features) 
- `stylelint` is based on my own profiles I am using in most of my projects ([@considonet/stylelint-config-bem](https://www.npmjs.com/package/@considonet/stylelint-config-bem), [@considonet/stylelint-config-order](https://www.npmjs.com/package/@considonet/stylelint-config-order), [@considonet/stylelint-config-default](https://www.npmjs.com/package/@considonet/stylelint-config-default))
- `eslint` is used to lint both JS and TS files. As of 2019 This setup [is supposed](https://eslint.org/blog/2019/01/future-typescript-eslint) to be the recommended one. My preferred setup included.

## Testing strategy

Because all the data-processing operations (such as filtering or sorting) are done on the API side, the tests cannot be done here. For this reason I have decided to add some simple tests to check if API querying works as supposed, if data models are mapped properly and if date values are interpreted in the right way. I have also added a simple `enzyme` test to determine whether the `Article` component properly displays all the information.

Please consider them as just a showcase, not a real-life scenario.

## Running

1. `npm run install` or `yarn` - to install dependencies
2. `npm run serve` or `yarn run serve` - to run the development server
3. `npm run build` or `yarn run build` - to build the production bundle
