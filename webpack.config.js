const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const webpack = require("webpack");

module.exports = env => {

  const isProduction = typeof env !== "undefined" && typeof env.production !== "undefined" && env.production;
  const sourceMap = !isProduction;

  return {
    devServer: {
      contentBase: path.join(__dirname, "dist"),
      compress: true,
      clientLogLevel: "silent",
      port: 8080,
      hot: true
    },
    entry: "./src/js/index.js",
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            "style-loader",
            {
              loader: "css-loader",
              options: {
                modules: false,
                importLoaders: 2,
                sourceMap
              }
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap
              }
            },
            {
              loader: "sass-loader",
              options: {
                implementation: require("sass"),
                sourceMap
              }
            }
          ]
        },
        {
          test: /\.[jt]sx?$/,
          exclude: file => (
            /node_modules/.test(file)
          ),
          use: ["babel-loader"]
        },
        {
          test: /\.(png|jpe?g|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {},
            }
          ],
        }
      ]
    },
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "app.js"
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: "AppUnite Interview Task",
        template: "./src/ejs/template.ejs"
      }),
      new webpack.HotModuleReplacementPlugin({}),
      new ForkTsCheckerWebpackPlugin({
        async: true,
        silent: false
      })
    ],
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx"]
    }
  }
};
